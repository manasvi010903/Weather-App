
# Weather App

This is a simple weather application that displays current weather information based on the user's input location.

## Features

- **Weather Display**: Shows weather icon, temperature, and description.
- **Additional Details**: Displays humidity and wind speed.

## Technologies Used

- HTML
- CSS
- JavaScript (Fetch API for data retrieval)

## APIs Used

- OpenWeatherMap API: Used to fetch current weather data based on location input.

## Getting Started

To use the weather app:

1. Clone this repository.
2. Open `index.html` in a web browser.

## Usage

1. Enter the location (city name) in the input field.
2. Click on the search button or press Enter.
3. The weather information will be displayed along with the corresponding background image.

## Screenshots
1. ![image](https://github.com/manasvi0109/Weather-App/assets/171707742/9a95cd78-5bbd-496e-806a-305384bf34ab)
2. ![image](https://github.com/manasvi0109/Weather-App/assets/171707742/43b8d241-1c43-4701-bd12-df34d02e4670)
3. ![image](https://github.com/manasvi0109/Weather-App/assets/171707742/4c4ff6a9-ba98-46d2-a91c-eedc680cf4c8)

